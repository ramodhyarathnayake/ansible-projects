provider "aws" {
  region     = "ap-southeast-1"
}

variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "avail_zone" {}
variable "env_prefix" {}
variable "my_ip" {}
variable "instance_type" {}
variable "public_key_value" {}
variable "ssh_key_private" {}


resource "aws_vpc" "myapp_vpc" {
  cidr_block = var.vpc_cidr_block

    tags = {
      Name = "${var.env_prefix}-vpc"}
}

resource "aws_subnet" "myapp_subnet_1" {
  vpc_id     = aws_vpc.myapp_vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.avail_zone

  tags = {
    Name = "${var.env_prefix}-subnet-1"
  }
}
#Resource: aws_internet_gateway
#Provides a resource to create a VPC Internet Gateway.
resource "aws_internet_gateway" "myapp-igw"{
  vpc_id = aws_vpc.myapp_vpc.id
  tags = {
    Name = "${var.env_prefix}-igw"
    }
}

#if we are using default route table we dont have to speicify the subnet assosiation because AWS automaticcaly assing avaiable 
#subnet to default route table

resource "aws_default_route_table" "main-rtb"{
  default_route_table_id = aws_vpc.myapp_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
      Name : "${var.env_prefix}-main-rtb"
  }
  }

#use this configuration if using seperate route table other than default route table
#Resource: aws_route_table 
#Provides a resource to create a VPC routing table

/*resource "aws_route_table""myapp_route_table"{
  vpc_id = aws_vpc.myapp_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }
    tags = {
    Name = "${var.env_prefix}-rtb"
  }*/


#Resource: aws_route_table_association
#Provides a resource to create an association between a route table and a subnet or a route table and an internet gateway or virtual private gateway.

/*resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id      = aws_subnet.myapp_subnet_1.id
  route_table_id = aws_route_table.myapp_route_table.id
}*/


resource "aws_default_security_group" "myapp-default-sg" {
  vpc_id      = aws_vpc.myapp_vpc.id

    ingress {
    description      = "Allwo SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.my_ip] 
    #who is allowd to access resource on port 22
    #ipv6_cidr_blocks = [aws_vpc.main.ipv6_cidr_block]
  }

    ingress {
    description      = "Allwo Web browser port"
    from_port        = 8000
    to_port          = 8000
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
}
    egress{
    description      = "Need to allow internet to the servers"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    prefix_list_ids = []
/*Usage With Prefix List IDs
Prefix Lists are either managed by AWS internally, or created by the customer using a Prefix List resource. 
Prefix Lists provided by AWS are associated with a prefix list name, or service name, that is linked to a specific region. 
*/
    }

    tags = {
    Name = "${var.env_prefix}-deafult-sg"  
    }
}


data "aws_ami" "latest_aws_linux_os" {
  most_recent      = true
  owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

output "aws_ami"{
  value = data.aws_ami.latest_aws_linux_os.id
}

resource "aws_key_pair""ssh-key"{
  key_name = "terraform-aws-key"
  public_key = var.public_key_value
}

output "Public_ip_address" {
  value       = resource.aws_instance.myapp_server.public_ip
  description = "The public IP address of the main server instance."
}


resource "aws_instance" "myapp_server" {
  ami           = data.aws_ami.latest_aws_linux_os.id
  instance_type = var.instance_type
  subnet_id = aws_subnet.myapp_subnet_1.id
  vpc_security_group_ids = [aws_default_security_group.myapp-default-sg.id]
  availability_zone = var.avail_zone
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name
#  count = 2
#  user_data = file("entry-script")
    tags = {
	Name = "${var.env_prefix}-ec2-server" 
#   Name = "${var.env_prefix}-ec2-server-${count.index}"  
    }


#	provisioner "local_exec"
#		working_dir = "C:\MyDevOpsProject\Ansible Projects\Ansible_docker\playbook.yaml"
#		command = " ansible-playbook --inventory ${self.public_ip} ${var.ssh_key_private} --user ec2-user playbook.yaml "
#	}
#}    

#https://docs.ansible.com/ansible/2.9/modules/wait_for_module.html

resource "null_resources" "Configure-server" {
	trigger = { trigger = aws_instance.myapp-server.public_ip}
	
	provisioner "local_exec"
		working_dir = "\Ansible_docker\playbook.yaml"
		command = " ansible-playbook --inventory ${aws_instance.myapp-server.public_ip} ${var.ssh_key_private} --user ec2-user playbook.yaml "
	}
}    


